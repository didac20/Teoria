/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sobrecarregasemafor;

/**
 *
 * @author alumne
 */
public class SobrecarregaSemafor {
//aixo existeix encara que no hagui fet un new semafor
//static serveix per inicialitzar sense fer un new semafor

    static final int VERMELL = 0;
    static final int GROC = 1;
    static final int VERD = 2;

//aixo no existeix si no faig un new semafor
//    final int VERMELL = 0;
//    final int GROC = 1;
//    final int VERD = 2;
    private int color = VERD;
    private boolean parpallejar = false;

    public SobrecarregaSemafor() {//constructor buit per defecte

    }
 
    public SobrecarregaSemafor(int color, boolean parpallejar) {//constructor on s'han de passar tots els parametres

        this.color = color;
        this.parpallejar = parpallejar;
    }

    public SobrecarregaSemafor(int color) {//constructor on nomes s'ha de passar el color automaticament si es groc parpalleja el posa a true
        this.color = color;
        if (color == GROC) {
            parpallejar = true;
        }

    }

    public SobrecarregaSemafor(boolean parpalleja) {//constructor on passes la variable parpallejar i si es true posa el color en groc
        if (parpalleja == true) {
            this.color = GROC;
        }
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public boolean isParpallejar() {
        return parpallejar;
    }

    public void setParpallejar(boolean parpallejar) {
        this.parpallejar = parpallejar;
    }

    public void canviar(int color, boolean parpallejar) {
        this.color = color;
        this.parpallejar = parpallejar;

    }

    public void canvair() {
        switch (color) {
            case VERD:
                color = GROC;
                parpallejar = true;
                break;
            case VERMELL:
                color = VERD;
                break;
            default:
                color = VERMELL;
                parpallejar = false;
                break;
        }
    }

}
