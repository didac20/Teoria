/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fitxer_binari;

import java.util.Calendar;

/**
 *
 * @author alumne
 */
public class persones {
    String nom;
    int edat;
    Calendar dataNeixament;
    double alcada;

    public persones(String nom, int edat, Calendar dataNeixament, double alcada) {
        this.nom = nom;
        this.edat = edat;
        this.dataNeixament = dataNeixament;
        this.alcada = alcada;
    }

    persones() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getEdat() {
        return edat;
    }

    public void setEdat(int edat) {
        this.edat = edat;
    }

    public Calendar getDataNeixament() {
        return dataNeixament;
    }

    public void setDataNeixament(Calendar dataNeixament) {
        this.dataNeixament = dataNeixament;
    }

    public double getAlcada() {
        return alcada;
    }

    public void setAlcada(double alcada) {
        this.alcada = alcada;
    }
    
}
