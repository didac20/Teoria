/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehicles;

/**
 *
 * @author alumne
 */
public class Vehicle {

    private String matricula;
    private int potencia;

    public Vehicle(String matricula, int potencia) {
        this.matricula = matricula;
        this.potencia = potencia;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public int getPotencia() {
        return potencia;
    }

    public void setPotencia(int potencia) {
        this.potencia = potencia;
    }

    @Override
    public String toString() {
        return "Vehicle{" + "matricula=" + matricula + ", potencia=" + potencia + '}';
    }

}
