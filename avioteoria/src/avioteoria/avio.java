/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package avioteoria;

/**
 *
 * @author alumne
 */
public class avio {

    private String marca;

    private int lCombustible=0;

    private String matricula;

    private String model;

    private int nSeients;

    public avio(String marca, String matricula, String model, int nSeients) {
        this.marca = marca;
        this.matricula = matricula;
        this.model = model;
        this.nSeients = nSeients;
    }

    /** 
     * Get the value of marca
     *
     * @return the value of marca
     */
    public String getMarca() {
        return marca;
    }

    /**
     * Set the value of marca
     *
     * @param marca new value of marca
     */
    public void setMarca(String marca) {
        this.marca = marca;
    }

    /**
     * Get the value of matricula
     *
     * @return the value of matricula
     */
    public String getMatricula() {
        return matricula;
    }

    /**
     * Set the value of matricula
     *
     * @param matricula new value of matricula
     */
    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    /**
     * Get the value of model
     *
     * @return the value of model
     */
    public String getModel() {
        return model;
    }

    /**
     * Set the value of model
     *
     * @param model new value of model
     */
    public void setModel(String model) {
        this.model = model;
    }

    /**
     * Get the value of nSeients
     *
     * @return the value of nSeients
     */
    public int getnSeients() {
        return nSeients;
    }

    /**
     * Set the value of nSeients
     *
     * @param nSeients new value of nSeients
     */
    public void setnSeients(int nSeients) {
        this.nSeients = nSeients;
    }

    /**
     * Get the value of lCombustible
     *
     * @return the value of lCombustible
     */
    public int getlCombustible() {
        return lCombustible;
    }

    /**
     * Set the value of lCombustible
     *
     * @param lCombustible new value of lCombustible
     */
    public void setlCombustible(int lCombustible) {
        this.lCombustible = lCombustible;
    }

    public void omplirDiposit() {
        lCombustible = 1000;
    }

    public void buidarDiposit() {
        lCombustible = 0;
    }

    public void gastarCombustible(int litres) {
        lCombustible -= litres;
    }

    avio copia() {
        avio copiaAvio=new avio(marca, matricula, model, nSeients);
                copiaAvio.setlCombustible(lCombustible);
                return copiaAvio;
    }
}
