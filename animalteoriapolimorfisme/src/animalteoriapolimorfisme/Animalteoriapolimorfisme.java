/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animalteoriapolimorfisme;

/**
 *
 * @author alumne
 */
public class Animalteoriapolimorfisme {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Animal[] animals = new Animal[5];

        animals[0] = new Gat();
        animals[1] = new Animal();
        animals[2] = new Gos();
        animals[3] = new Gat();
        animals[4] = new Gos();
        
        for (int i = 0; i < animals.length; i++) {
            System.out.println(animals[i]);
        }
    }

}
