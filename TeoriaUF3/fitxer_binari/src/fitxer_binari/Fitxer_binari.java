/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fitxer_binari;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 *
 * @author alumne
 */
public class Fitxer_binari {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException, IOException {

        Fitxer_binari programa = new Fitxer_binari();
        programa.inici();

    }

    private void inici() throws FileNotFoundException, IOException {

        File f = new File("fitxer.dat");
        RandomAccessFile fitxer = new RandomAccessFile(f, "rw");
        

        persones p = new persones("Pep", 25, new GregorianCalendar(1995, Calendar.FEBRUARY, 20), 1.71);

        gravar(p, fitxer);
        fitxer.seek(0);
        persones p2 = llegirPersona(fitxer);
        mostrar(p2);
        fitxer.close();
    }

    private void gravar(persones p, RandomAccessFile fitxer) throws IOException {
        
        if (p.getNom().length()>20) {//primer s'ha de comprobar si el nom fa mes de 20 caracters
            p.getNom().substring(0, 20);//retalla el nom quan sigui mes de 20 caracters
        }
        
        fitxer.writeBytes(String.format("%20s\n", p.getNom()));//grava el nom a l'arxiu i fa que els nom es guardin amb una mida de 20
        fitxer.writeInt(p.getEdat());
        fitxer.writeLong(p.getDataNeixament().getTimeInMillis());
        fitxer.writeDouble(p.getAlcada());

    }

    private persones llegirPersona(RandomAccessFile fitxer) throws IOException {
        String nom = fitxer.readLine().trim();
        int edat = fitxer.readInt();
        GregorianCalendar data = new GregorianCalendar();
        data.setTimeInMillis(fitxer.readLong());
        double alcada = fitxer.readDouble();
        persones p = new persones(nom, edat, data, alcada);
        return p;
    }

    private void mostrar(persones p) {
        System.out.println("Nom: " + p.getNom().trim());// mostra el nom i esborra els espais en blanc
        System.out.println("Data: "+p.getDataNeixament().getTime());
        System.out.println("Edat: " + p.getEdat());
        System.out.println("Alçada: " + p.getAlcada());
    }

}
