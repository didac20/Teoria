/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fitxerskk;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Scanner;

/**
 *
 * @author alumne
 */
public class Fitxerskk {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        // TODO code application logic here
        Fitxerskk f = new Fitxerskk();
        f.inici();
    }

    private void inici() throws IOException {
        File fitxer = new File("fitxer.txt"); //crear el fitxer
        File fitxersortida = new File("fitxersortida.txt");
        
        Scanner input = new Scanner(fitxer); //crear el input per poder mirar el fitxer
        PrintStream output = new PrintStream(fitxersortida);//escriure a un fitxer
        
        int contador=0;
        double mitjana=0;
        while(input.hasNextInt()){
            mitjana+=input.nextInt();
            contador++;
        }
        mitjana=mitjana/contador;
        output.println("Mitjana: "+mitjana);//escriure la mitjana al fitxer
        System.out.println(mitjana);
        input.close();
        
        

//        while (input.hasNextInt()) { //llegir tots els numeros del fitxer
//            System.out.println(input.nextInt());
//        }

        input.close(); //tancar el fitxer
//        System.out.println("Carpeta de treball: "+System.getProperty("user.dir"));
//        
//        System.out.println(fitxer.exists());
//        fitxer.createNewFile();
//        System.out.println(fitxer.exists());
//        
//        System.out.println(fitxer.getAbsolutePath());
//        System.out.println(fitxer.getCanonicalPath());
//        System.out.println(fitxer.getName());
//        System.out.println(fitxer.getTotalSpace());
//        System.out.println(fitxer.getParent());
//        System.out.println(fitxer.length());

    }

}

