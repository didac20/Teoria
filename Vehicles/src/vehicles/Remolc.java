/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehicles;

/**
 *
 * @author alumne
 */
class Remolc {
    private int volum;
   private int mma;

    public Remolc(int volum, int mma) {
        this.volum = volum;
        this.mma = mma;
    }

    public int getVolum() {
        return volum;
    }

    public void setVolum(int volum) {
        this.volum = volum;
    }

    public int getMma() {
        return mma;
    }

    public void setMma(int mma) {
        this.mma = mma;
    }

    @Override
    public String toString() {
        return "Remolc{" + "volum=" + volum + ", mma=" + mma + '}';
    }
    
    
}
