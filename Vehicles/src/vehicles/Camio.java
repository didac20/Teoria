/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehicles;

/**
 *
 * @author alumne
 */
public class Camio extends Vehicle {

    private Cabina cabina;
    private Remolc remolc;

    public Camio(Cabina cabina, Remolc remolc, String matricula) {
        super(matricula, cabina.getPotencia());
        this.cabina = cabina;
        this.remolc = remolc;
    }

    @Override
    public void setPotencia(int potencia) {
        super.setPotencia(potencia); //To change body of generated methods, choose Tools | Templates.
        cabina.setPotencia(potencia);
    }

    public Cabina getCabina() {
        return cabina;
    }

    public void setCabina(Cabina cabina) {
        this.cabina = cabina;
    }

    public Remolc getRemolc() {
        return remolc;
    }

    public void setRemolc(Remolc remolc) {
        this.remolc = remolc;
    }

    @Override
    public String toString() {
        return super.toString()+"Camio{" + "cabina=" + cabina + ", remolc=" + remolc + '}';
    }

    

}
