/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehicles;

/**
 *
 * @author alumne
 */
public class Cotxe extends Vehicle {//extencio de la calse vehicle

    private int seients;

    public Cotxe(int seients, String matricula, int potencia) {
        super(matricula, potencia);//cridada del constructor de la superclase
        this.seients = seients;
    }

    public int getSeients() {
        return seients;
    }

    public void setSeients(int seients) {
        this.seients = seients;
    }

    @Override
    public String toString() {
        return super.toString()+"Cotxe{" + "seients=" + seients + '}';
    }

}
