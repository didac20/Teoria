/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dateskk;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;



/**
 *
 * @author alumne
 */
public class Dateskk {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Calendar dataInici=new GregorianCalendar(2020, Calendar.JANUARY,23);
        dataInici.set(2021, Calendar.JANUARY, 12);
        
        Calendar dataActual=Calendar.getInstance();
        System.out.println("Any: "+dataActual.get(Calendar.YEAR));
        System.out.println("Mes: "+dataActual.get(Calendar.MONTH)+1);
        System.out.println("Dia: "+dataActual.get(Calendar.DAY_OF_MONTH));
        
        if (dataInici.before(dataActual))
            System.out.println("La data d'inici es anteriror a l'actual");
        else 
            System.out.println("La data d'inici es igual o posteriro a l'actual");
        
        SimpleDateFormat formatData=new SimpleDateFormat("dd/MMM/yyyy");
        
        System.out.println(formatData.format(dataInici.getTime()));
        //Convertir cadena a calendar
        String cadenaData="01/01/2020";
        
        Calendar data=new GregorianCalendar();
        Date dataConvertida;
        try {
            dataConvertida = formatData.parse(cadenaData);
        } catch (ParseException ex) {
            dataConvertida=new Date(0, 0, 0);
        }
        data.setTime(dataConvertida);
        
        //Convertir calendar a cadena
        
        cadenaData=formatData.format(data.getTime());
        
    }
    
}
