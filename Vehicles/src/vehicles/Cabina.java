/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehicles;

/**
 *
 * @author alumne
 */
class Cabina {
   private int potencia;
   private int seients;

    public Cabina(int potencia, int seients) {
        this.potencia = potencia;
        this.seients = seients;
    }

    public int getPotencia() {
        return potencia;
    }

    public void setPotencia(int potencia) {
        this.potencia = potencia;
    }

    public int getSeients() {
        return seients;
    }

    public void setSeients(int seients) {
        this.seients = seients;
    }

    @Override
    public String toString() {
        return "Cabina{" + "potencia=" + potencia + ", seients=" + seients + '}';
    }
    
    
}
